"use strict"

const bbrFiles = require('./bombarder-files');
const Q = require('q');
const path = require('path');

var tests = {
	tests: [],
	default: ''
}

class BombarderTests {

	constructor () {

	}

	init () {
		const deferred = Q.defer();

		bbrFiles.load(bbrFiles.testsIndex)
		.then((data) => {
			tests = data;
			deferred.resolve()
		}).catch((err) => {
			deferred.resolve()
		//	console.log(err)
		})

		return deferred.promise
	}

	add (name, path) {
		var test = { name: name, path: path }
		tests.tests.push(test);
		if (tests.tests.length === 1) {
			tests.default = name;
		}

		bbrFiles.write (bbrFiles.testsIndex, JSON.stringify(tests))
	}

	remove (name) {
		var ret = false;
		for (let i = 0; i < tests.tests.length; i++) {
			if (tests.tests[i].name === name) {
				if (name === tests.default) {
					tests.default = '';
				}

				tests.tests.splice(i, 1);
				i--;	
				ret = true;
			}
		}

		bbrFiles.write (bbrFiles.testsIndex, JSON.stringify(tests));

		return ret;
	}

	setDefault (name) {
		tests.default = name;
		bbrFiles.write (bbrFiles.testsIndex, JSON.stringify(tests))
	}

	getDefault () {
		return tests.default;
	}

	getTestByIndex (index) {
		if ( tests.tests.length > index && index > -1) {
			return tests.tests[index]
		}
	}

	getTestByName (name) {
		for (let i = 0; i < tests.tests.length; i++) {
			if (tests.tests[i].name === name) {
				return tests.tests[i];
			}
		}
	}

	names (all) {
		return tests.tests.map((test) => {
			if (all) {
				return test.name + '\t' + test.path;
			} else {
				return test.name;
			}
		});
	}



	load (name) {		
		name = name || tests.default;
		const deferred = Q.defer();
		var test;
	
		for (let i = 0; i < tests.tests.length; i++) {
			if (tests.tests[i].name === name) {
				test = tests.tests[i];
				break;
			}
		}

		if (test) {
			bbrFiles.load(test.path).then((t) => {
				t.path = test.path;
				var dirName = path.dirname(t.path)
				if (t.files) {
					for (let file in t.files) {
						if (t.files.hasOwnProperty(file)) {
							t.files[file] = path.resolve(dirName, t.files[file]); 
						}
					}
				}

				deferred.resolve(t);
			})
		} else {
			deferred.reject();
		}

		return deferred.promise;
	}
}

/*var t = new BombarderTests();

t.init().then(() => {
//	console.log(tests)
//	t.add('sdfsdfsdf/sdfsdfs.fff', 'gggg');

	console.log(t.names())
})*/

module.exports = new BombarderTests();