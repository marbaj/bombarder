"use strict"

const request = require('request');
const fs = require('fs');
const Endpoint = require('./endpoint');
const Bomb = require('./bomb');
const Q = require('q');
const bbrFiles = require('./bombarder-files');

const Bombarder = function(task) {
	var task = task;	
	const that = this;
	this.intervals = [];

	// Number of requests per second
	const requestPerSec = task.request_per_second ? parseFloat(task.request_per_second) : 1;

	const bomb = new Bomb(task.host);	

	var endpoints = task.endpoints.map((item, index) => {
		const time_window = parseFloat(task.time_window || -1);
	
		return new Endpoint(item, requestPerSec);
	});

	this.timeWondow = () => {
		return parseFloat(task.time_window || -1);
	};

	this.openConns = () => {
		return bomb.openConns();
	}

	this.init = () => {
		const deferred = Q.defer();
		const files = Object.keys(task.files || {});

		const qs = files.map((file) => {
			return bbrFiles.load(task.files[file])
		});

		Q.allSettled(qs).then((results) => {
			const in_files = results.map((result, index) => {
				if (result.state === 'fulfilled') {
		            const o = {};
		            o[files[index]] = result.value;

		            return o;
		        }
			});

			var new_ep = [];

			endpoints.forEach((ep) => {	
				var t = ep.inputData(in_files);
				if (t && t.length > 0) {
					new_ep = new_ep.concat(t);
				}		
			});

			endpoints = new_ep;

			that.drop();

			deferred.resolve();
		});

		return deferred.promise;
	};

	this.drop = () => {
		const runAll = () => {
			//Create consurant runs
			const concurrent = task.concurrent || [];
			const runs = endpoints.slice(0);

			concurrent.forEach((item) => {
				//check if array
				if (item.endpoints && Array.isArray(item.endpoints) && item.endpoints.length > 0) {	
					const rps = item.request_per_second ? parseFloat(item.request_per_second) : requestPerSec;
					const mill = 1000 / rps;
					const c = { 
						concurrents: true, 
						requestPerSec: rps, 
						reqMillSec: mill 
					}

					c.endpoints = item.endpoints.map((endPointName) => {
						// remove endpoint from runs list
						for (let i = 0; i < runs.length; i++) {
							if (runs[i].name === endPointName) {	
								const ep = runs[i];
								ep.request_per_second = c.request_per_second;
								ep.reqMillSec = c.reqMillSec;
								runs.splice(i, 1);

								return ep;
							}
						}
					});

					runs.push(c);
				}
			});

			if (task.cycle) {
				var ind = 0;
				var f = 1000 / requestPerSec;
				var interval = setInterval(() => {
					ind = ind < runs.length ? ind : 0;
					bomb.dropOnTarget(runs[ind], ind);
					ind++;
				}, f);

				that.intervals.push(interval);	
			} else {
				runs.forEach((run) => {
					var interval;
					if (run.concurrents === true) {
						interval = setInterval(() => {
							run.endpoints.forEach((ep) => {
								if (ep.execute) {
									bomb.dropOnTarget(ep);
								}
							});		
						}, run.reqMillSec);	
					} else {
						if (run.execute) {
							interval = setInterval(() => {
								bomb.dropOnTarget(run);
							}, run.reqMillSec);	
						}
					}

					if (interval) {
						that.intervals.push(interval);			
					}
				});
			}
		};

		const auth = findAuthEndpoint(task);

		if (auth) {
			auth.execute = false;
			bomb.authenticate(auth)
			.then((data) => { 
				runAll();
			});
		} else {
			runAll();
		}		

		function findAuthEndpoint(task) {
			if (task.authentication) {
				for (let i = 0; i < endpoints.length; i++) {
					if (endpoints[i].name === task.authentication.endpoint_name) {
						return endpoints[i];
					}
				}
			}
		}
	};

	this.stop = () => {
		this.intervals.forEach((item) => {
			stopInterval(item);
		});

		this.intervals = [];
	};

	this.status = () => {
		
	};

	this.endpoints = () => {
		return endpoints;		
	};

	this.deleteCookies = () => {
		fs.unlinkSync(bbrFiles.jar);
		fs.writeFileSync(bbrFiles.jar, '');
	};

	this.testName = () => {
		return task.name;
	}
};

module.exports = Bombarder;
