"use strict";

const fs = require('fs');
const Q = require('q');
const extend = require('util')._extend;
const objectQuery = require('object-string-query');

class Endpoint {

	constructor(config, requestPerSec) {
		this.config = config;
		this.execute = true;
		this.hasRan = false;
		requestPerSec = requestPerSec || 1;
		requestPerSec = config.request_per_second ? parseFloat(config.request_per_second) : requestPerSec;

		// time windows for thats in seconds
		this.timeWindow = config.time_window || -1;

		this.settings = {
			name: config.name || 'none',
			host: config.host,
			endpoint: config.endpoint || '',
			queryObject: config.queryObject,
			method: config.method || 'GET',
			body: config.body,
			bodyObject: config.bodyObject,
			qs: config.qs || '',
			requestPerSec: requestPerSec,
			reqMillSec: 1000 / requestPerSec
		};

		this.stats = {
			total_time: 0,
			last_time: 0,
			min: 0,
			max: 0,
			hits: 0,
			window_hits: 0,
			mean: 0,
			size: 0,
			errors: 0
		};

		this.responseBuffer = [];
	}

	inputData (data) {	
		if (!this.settings.bodyObject && !this.settings.queryObject) {
			return [this]
		}

		var result = [];
		data.forEach((d) => {
			var b = this.bodyObject(d);
			if (b) {
				result = result.concat(b);
			}

			var q = this.queryObject(d);
			if (q) {
				result = result.concat(q);				
			}
		})

		return result;
	}

	bodyObject (data) {
		var s = this.settings.bodyObject;
		if (!s) {
			return;
		}

		try {
			var obj = objectQuery.parse(s, data);		
			if (obj) {
				if (Array.isArray(obj)) {
					let fork = [];
					for (let i = 0; i < obj.length; i++) {
						let t = obj[i];
						let ep = new Endpoint(this.config, this.settings.requestPerSec);
						ep.settings.name += t.name || (ep.settings.name + '-' + i);
						ep.settings.body = t.body || t;

						fork.push(ep);
					}

					return fork;
				} else {
					this.settings.body = obj;
					return this;
				}
			}			
		} catch (e) {
		}
	}

	queryObject (data) {
		var s = this.settings.queryObject;		
		if (!s) {
			return;
		}

		try {
			var obj = objectQuery.parse(s, data);		
			if (obj) {
				if (Array.isArray(obj)) {
					let fork = [];
					for (let i = 0; i < obj.length; i++) {
						let t = obj[i];
						let ep = new Endpoint(this.config, this.settings.requestPerSec);
						ep.settings.name +=  t.name || (ep.settings.name + '-' + i);
						ep.settings.endpoint = t.query || t;

						fork.push(ep);
					}  

					return fork;
				} else {
					this.settings.endpoint = obj;
					return this;
				}
			}			
		} catch (e) {
		}
	}

	response (time, response, body) {
		this.hasRan = true;

		const size = Buffer.byteLength(body, 'utf8') / 1024 // size in Kbs

		this.stats.last_time = time;
		this.stats.total_time += time;
	  	this.stats.hits++;
	  	this.stats.window_hits;

	  	this.stats.min = this.stats.min == 0 ? time : this.stats.min
	  	this.stats.min = this.stats.min > time ? time : this.stats.min
	  	this.stats.max = this.stats.max < time ? time : this.stats.max
	  	this.stats.size += size

	  	if (this.timeWindow > 0) {
		  	let current = Date.now();
		  	let cutOff = current - this.timeWindow*1000

		  	this.responseBuffer.push({ time: time, size: size, date: current });

		  	while (this.responseBuffer[0].date < cutOff) {
		  		this.stats.total_time -= this.responseBuffer[0].time;
		  		this.stats.mean = this.stats.total_time / this.responseBuffer.length;
		  		this.stats.size -= this.responseBuffer[0].size;

		  		this.responseBuffer.shift();
		  	}

		  	this.stats.window_hits = this.responseBuffer.length;

		  	this.stats.mean = this.stats.total_time / this.responseBuffer.length;		
	  	} else {
	  		this.stats.mean = this.stats.total_time / this.stats.hits;
	  		this.stats.window_hits = this.stats.hits
	  	}
	}

	set id (id) {
		this.id = id
	}

	get id () {
		return this.settings.id
	}

	get size () {
		return Math.floor(this.stats.size)
	}

	get mean () {
		return Math.floor(this.stats.mean)
	}

	get max () {
		return Math.floor(this.stats.max)
	}

	get min () {
		return Math.floor(this.stats.min)
	}

	get hits () {
		return this.stats.window_hits;
	}

	get name () {
		return this.settings.name;
	}

	get reqMillSec () {
		return this.settings.reqMillSec;
	}

	get requestPerSec () {
		return this.settings.requestPerSec;
	}

	set reqMillSec (value) {
		this.settings.reqMillSec = value;
	}

	set requestPerSec (value) {
		this.settings.requestPerSec = value;
	}

	get qs () {
		return this.settings.qs;
	}

	get method () {
		return this.settings.method;
	}

	get hasData () {
		return this.hasRan;
	}

	error () {
		this.stats.errors++;
	}

	endpoint () {
		var ep;
		if (Array.isArray(this.settings.endpoint) && this.settings.endpoint.length > 0) {
			this.settings.currentEndpoint = this.settings.currentEndpoint || 0;
			this.settings.currentEndpoint = this.settings.endpoint.length > this.settings.currentEndpoint ? this.settings.currentEndpoint : 0;
			body = this.settings.endpoint[this.settings.currentEndpoint];

			this.settings.currentEndpoint++;
		} else {
			ep = this.settings.endpoint;
		}

		return ep;	
	}
	
	/*
	 * Returns request body for this end point. 
	 * If it is an array then loop and always return next
	 */
	body () {
		var body;
		if (Array.isArray(this.settings.body) && this.settings.body.length > 0) {
			this.settings.currentBody = this.settings.currentBody || 0;
			this.settings.currentBody = this.settings.body.length > this.settings.currentBody ? this.settings.currentBody : 0;
			body = this.settings.body[this.settings.currentBody];

			this.settings.currentBody++;
		} else {
			body = this.settings.body;
		}

		return JSON.stringify(body);
	}
}

module.exports = Endpoint;
