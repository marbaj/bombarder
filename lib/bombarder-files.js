"use strict"

const fs = require('fs');
const Q = require('q');
const path = require('path');

class BbrFiles {
 
	constructor () {
		this.create(this.folder);
		this.create(this.jar, true);
		this.create(this.testsFolder);
		this.create(this.testRunsFolder);
		this.create(this.testsIndex, true);
	}

	create (path, file, text) {
		if (!fs.existsSync(path)) {
			if (!file) {
				fs.mkdirSync(path);
			} else {
				text = text || '';
				fs.writeFileSync(path, text);
			}
		}
	}

	write (path, text) {
		text = text || '';
		fs.writeFileSync(path, text);
	}

	get jar () {
		return path.join(this.folder, '/cookies.json');
	}

	get folder () {
		return '.././.bombarder';
	}

	get testsFolder () {
		return path.join(this.folder, '/tests'); 
	}

	get testRunsFolder () {
		return path.resolve(path.join(this.folder, '/runs')); 
	}

	get testsIndex () {
		return path.join(this.folder, '/tests/index.txt');
	}

	load (fileName) {
		const deferred = Q.defer();
		const that = this;

		fs.readFile(fileName, (err, data) => {
			if (!err) {	
				try {
					let d = JSON.parse(data.toString());
					deferred.resolve(d);
				} catch (e) {
					deferred.reject(e);
				}
			} else {
				deferred.reject();
			}
		});
		
		return deferred.promise;
	}

	setDefaultTest (test) {
		fs.writeFileSync(this.defaultTest, test);
	}
} 

module.exports = new BbrFiles();