"use strict"

const Bombareder = require('./bombarder');
const program = require('commander');
const bbrTests = require('./bombarder-tests');
const Q = require('q');
const path = require('path');
const defaultReporter = '.././reporters/bombarder-cli';
const BbrServer = require('.././interface/bbrServer')
var reportes = [];


bbrTests.init().then(() => {

	program
	  .version('0.0.1')
	
	/*program
	  .command('cookies [cmd]')
	  .option('-d, --delete', 'remove all cookies')
	  .action(function(cmd, options) {
	  	bombareder.deleteCookies()
	    console.log('exec "%s" using %s mode', cmd, options.delete);
	  //   console.log(options);
	  })*/

	 program
	  .command('tests [cmd] [params...]')
	  .description('') 
	  .option('-a, --all', 'show test names and config file paths')
	  .action(function(cmd, params, options) {
			if (!cmd) {
				console.log('default test: %s',  bbrTests.getDefault())
			} else if (cmd == 'list') {
				console.log('\n');
				bbrTests.names(options.all).forEach((name) => {
					console.log('\t%s', name);
				});						
				console.log('\n')
			} else if (cmd === 'add' && params.length === 2) {
				let fileNamePath = path.resolve(params[1]);
				bbrTests.add(params[0], fileNamePath);
			} else if (cmd === 'remove' && params.length === 1) {
				bbrTests.remove(params[0]);
			} else if (cmd === 'set' && params.length === 1) {
				bbrTests.setDefault(params[0]);
			}
	  });

	program
	  .command('run [test name]')
	  .description('') 
	  .action(function(cmd, options) {
			var test = cmd ? cmd : bbrTests.getDefault();
			if (test && test !== '') {
				bbrTests.load(test).then((config) => {
					const bombareder = new Bombareder(config);

					// Load reporters from config or use default CLI reporter
					const customReporters = config.reporters || [defaultReporter];
					customReporters.forEach((reporter) => {	
						try {
							var Reporter = require(reporter);
							if (Reporter) {
								reportes.push(new Reporter()) 
							}
						} catch (e) {
							debugger;
						}
					});

					bombareder.init().then(() => {
						reportes.forEach((reporter) => {
							reporter.write(bombareder);
						});
					}).catch((e) => {
						debugger
					});
				}).catch(() => {

				});				
			} else {
				console.log('\n \t no default test name set. set test name vis: tests set <name> \n')
			}
	  })
	program
	  .command('server [port]')
	  .action(function(cmd, options) {
	  	let port = 9001;
	  	if (cmd) {
	  		port = parseInt(cmd);
	  	}
		
		var s = new BbrServer(port);
	  });

	program.parse(process.argv);
});