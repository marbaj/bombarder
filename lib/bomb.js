"use strict"

const request = require('request');
const url = require('url')
const Q = require('q');
const FileCookieStore = require('tough-cookie-filestore');
const bbrFiles = require('./bombarder-files');

var Bomb = function (host) {

	var openConnections = 0;

	var host = host;
	var config = {
		timeout: 120000,
	    headers: {
		    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
		    'Content-Type': 'application/json'
		}
	}
	
	this.authenticate = (endpoint) => {
		config.jar = request.jar(new FileCookieStore(bbrFiles.jar))
		
		if (config.jar.getCookieString(url.resolve(host, endpoint.endpoint))) {
			let deferred = Q.defer();
			deferred.resolve();

			return deferred.promise;
		} else {
			return this.dropOnTarget(endpoint);		
		}
	}

	this.openConns = () => {
		return openConnections;
	}

	this.dropOnTarget = (endpoint, index) => {
		var deferred = Q.defer();
		if (endpoint === undefined) {
			deferred.reject();
			return deferred.promise;
		}


		config.url = url.resolve(host, endpoint.endpoint())
		config.method = endpoint.method || 'GET';
	//	config.proxy = 'http://127.0.0.1:8888'

		if (endpoint.qs) {
			coreplnfig.qs = endpoint.qs;
		}
		
		var body = endpoint.body();
		if (body) {
			config.body = body;
		}

		var callback = ((startTime, ep) => {
			return (error, res, body) => {
				if (error) {
					ep.error(error);
				} else {
					ep.response(Date.now() - startTime, res, body);
				}

				openConnections--;

				deferred.resolve();
			}
		})(Date.now(), endpoint);

		request(config, callback);

		openConnections++;

		return deferred.promise;
	}
}

module.exports = Bomb;