"use strict"

const Bombareder = require('.././lib/bombarder');
const bbrFiles = require('.././lib/bombarder-files');
const fs = require('fs');
const path = require('path');

var Reporter = function() {	

	const tokans = {
		start: 'start_time',
		fields: 'report_fileds',
		timeStamp: 'time_stamp',
		startData: 'start_data',
		endData: 'end_data'
	}

	var getFilePath = (fileName) => {
		const folder = bbrFiles.testRunsFolder;
		return path.join(folder, fileName + '.bbr');
	}

	this.write = (bombareder, config) => {	

		config = config || { };
debugger
		const refreshInterval = config.freq || 2000;
		const outputs = config.ouputs || ['name', 'mean', 'max', 'min'];			
		const endpoints = bombareder.endpoints();
		const fileName = getFilePath(bombareder.testName());

		if (!fs.existsSync(fileName)) {
			let txt = tokans.start+'\n';
			txt += (new Date().toLocaleString())+'\n';
			txt += tokans.fields+'\n';

			outputs.forEach((out) => {
				txt += out + ',';
			});

			txt += '\n';

			fs.writeFileSync(fileName, txt);
		}
		
		setInterval(() => {
			var txt = tokans.timeStamp+'\n';
			txt += Date.now() + '\n';
			txt += tokans.startData + '\n';

			for (let i = 0; i < endpoints.length; i++) {
				outputs.forEach((out) => {
					txt += endpoints[i][out] + ','
				});
				txt += '\n';			
			}

			txt += tokans.endData+'\n';

			fs.appendFile(fileName, txt, function (err) {
				if (err) {
					throw err;
				}
			});

		}, refreshInterval);
	};

	this.read = (name, start, end) => {
		const fileName = getFilePath(name);
		if (!fs.existsSync(fileName)) {
			return;
		}

		const readline = require('readline');
		const instream = fs.createReadStream(fileName);
        const outstream = new (require('stream'))();
        const rl = readline.createInterface(instream, outstream);

        const rpt = [];
        var timeStamp;
        var section;
        var fields;
        var data ;
        
		rl.on('line', function(line) {

			switch (line) {
				case tokans.start:
				case tokans.fields:
				case tokans.timeStamp:
				case tokans.startData:
					section = line;
					data = false;
					break;
				case tokans.endData:
					section = line;
					data = false;
					rpt.push(timeStamp);
					timeStamp = null;
					break;	
				default:
					data = true;
					break;					
			}

			if (section === tokans.fields && data === true) {
				fields = line.split(',');
			} else if (section === tokans.timeStamp && data === true) {		
				timeStamp = {
					timeStamp: line,
					data: []
				}
			} else if (section === tokans.startData && data === true && timeStamp && fields) {
				let l = line.split(',');
				let struct = {};

				fields.forEach((out, index) => {
					struct[out] = l[index]
				});

				timeStamp.data.push(struct);
			}
		});

		rl.on('close', function() {
			rpt.forEach((r) => {
				console.log(r)
			});
		});
	}
}

/*var r = new Reporter();
r.read('es')
*/
module.exports = Reporter;