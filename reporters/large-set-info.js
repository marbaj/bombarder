"use strict"

const log = require('single-line-log').stdout;
const Bombareder = require('.././lib/bombarder');
const readline = require('readline');

var sortBy = 'mean';
var showItems = 20;
var acs = -1;
var flag = true;

const state = {
	acs: -1,
	flag: true
}

var getState = () => {
	return state;
}

var Reporter = function() {

	this.write = (bombareder) => {	
		const refreshInterval = 1000 // 1 second
		const endpoints = bombareder.endpoints();
		const fieldLen = 12;

		console.log('\nBombared load test stared at: %s', (new Date()).toLocaleString());
		console.log('Stats. time wondow: %s', bombareder.timeWondow());
		console.log('------------------------------------------------------------------------------------------------------------------');
		console.log('                                                   BOMBARDER                                                      ');
		console.log('------------------------------------------------------------------------------------------------------------------');
		console.log('Endpoint\tMean (ms)\tMax (ms)\tMin (ms)\tReceived (kb)\tReq/Sec\t\tHits\tErrors #');

		setInterval(() => {

			var msg = '';

			const state = getState();

			endpoints.sort((a, b) => {
				var result;

				if (a.hasData && b.hasData) {
					result = a[sortBy] - b[sortBy];
				} else if (a.hasData) {
					result = 1;
				} else {
					result = -1;
				}

				return acs*result;
			});

			for (let i = 0; i < endpoints.length; i++) {
				if (i > showItems) {
					return;
				}

				if (!flag) {
					break;
				}

				let ep = endpoints[i];

				var listName = ep.name;

				// Pad shorter names
				if (listName.length < fieldLen) {
					listName += Array(fieldLen - listName.length + 1).join(' ');
				}

				const lines = Math.floor(listName.length / fieldLen);
				const nameParts = [];

				for (let l = 0; l < lines; l++) {
					nameParts.push(listName.substring(l * fieldLen, fieldLen));
				}

				const rest = listName.length - lines * fieldLen;
				
				if (rest > 0) {
					nameParts.push(listName.substring(lines * fieldLen, lines * fieldLen + rest));
				}

				msg += `${state.acs}\n${nameParts[0]}\t${ep.mean}\t\t${ep.max}\t\t${ep.min}\t\t${ep.size}\t\t${ep.requestPerSec}\t\t${ep.hits}\t\t${ep.stats.errors}\n`;

				if (nameParts.length > 1) {
					for (let i = 1; i < nameParts.length; i++) {
						msg += nameParts[i] +'\n';
					}
				}
			};

			log(msg);
		}, refreshInterval);
	};
	
}



module.exports = Reporter;