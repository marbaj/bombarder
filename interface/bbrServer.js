"use strict"
const express = require('express');
const Bombareder = require('.././lib/bombarder');
const bbrTests = require('.././lib/bombarder-tests');
const reporter = require('.././reporters/file-stream-reporter');

const app = express();
var bombareder;

var BbrServer = function (port) {

	var status = () => {
		var s = {
			status: 'running'
		}

		if (bombareder) {
			s.testName = bombareder.testName();
			s.endpoints = bombareder.endpoints().length;
			s.coonection = bombareder.openConns();
		} else {
			s.status = 'not running';
		}

		return s;
	}

	app.get('/', function (req, res) {
		res.send('Bombareder!');
	});

	app.get('/tests', function (req, res) {
		var names = bbrTests.names();
		var r = names.map((name, index) => {
			return {
				name: name,
				index: index
			}
		})

		res.send(r);
	});

	app.get('/tests/default', function (req, res) {
		var name = bbrTests.getDefault();
		res.send(name);
	});

	app.get('/run', function (req, res) {
		var test
		var index = req.param('index') || '';
		index = parseInt(index);

		if (!isNaN(index)) {
			test = bbrTests.getTestByIndex(index);
		} else {
			test = bbrTests.getDefault();
		}

		if (!test) {
			 res.send('Error, no test to run!');
			return 
		}

		bbrTests.load(test).then((config) => {
			bombareder = new Bombareder(config);
			bombareder.init().then(() => {
				reporter.write(bombareder, { freq: 10000 });

				res.send('ok');
			}).catch((e) => {
				res.send(e);
			});	
		}).catch(() => {
			res.send('error');
		});	
	});

	app.get('/results', function (req, res) {
		if (!bombareder) {
			 res.send(status());
			 return;
		}

		const eps = bombareder.endpoints().slice();

		var sort = req.param('sort') || 'des';
		var start = req.param('skip') || '0';
		var take = req.param('take') || '10';
		var sortBy = req.param('sortby') || 'mean';
		var sig = -1;
		
		sortBy = sortBy || 'mean';
		start = parseInt(start)
		take = parseInt(take)

		if (sort === 'des') {
			sig = -1;
		} else if ('asc') {
			sig = 1;
		}

		eps.sort((a, b) => {
			var result;

			if (a.hasData && b.hasData) {	
				result = a[sortBy] - b[sortBy];
			} else if (a.hasData) {
				result = 1;
			} else {
				result = -1;
			}

			result *= sig;

			return result;
		});

		var results = [];

		start = start < eps.length ? start : eps.length;
		take += start;
		take = take < eps.length ? take : eps.length;

		for (let i = start; i < take; i++) {
			let ep = eps[i];
			let obj = {
				name: ep.name,
				mean: ep.mean,
				max: ep.max,
				min: ep.min,
				errors: ep.error(),
				hasData: ep.hasData
			}

			results.push(obj);
		}

		res.send(results);
	});

	app.get('/status', function (req, res) {
		 res.send(status());
	})

	app.get('/stop', function (req, res) {	
		if (bombareder) {
			bombareder.stop();
		} else if (!bombareder) {
			res.send(status());
		} 
	})

	app.listen(port, function () {
		console.log('Listening on port %s!', port);
	});
}

module.exports = BbrServer;

