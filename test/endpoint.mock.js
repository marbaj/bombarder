"use strict"

const http = require('http');

var MockServer = function (port) {
	const min = 100;
	const max = 150;

	var counter = 0;

	http.createServer((req, res) => {
		counter++;
		console.log(counter)
		const delay = Math.floor(Math.random() * (max - min)) + min;
		setTimeout(() => {
			res.writeHead(200);
			res.end('ok');
		}, delay);

	}).listen(port);

	this.stop = function() {
		http.close();
	}
}

var m = new MockServer(3000)

module.exports = MockServer;